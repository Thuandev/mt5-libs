#include <Math/Stat/Math.mqh>
#include <Trade/SymbolInfo.mqh>

class CFxceSymbol
{
private:
    CFxceSymbol(/* args */) {}
    ~CFxceSymbol() {}

public:
    /**
     * @brief Normalize volume with volume step's digits of symbol
     * 
     * @param volume Volume
     * @param symbol Symbol
     * @return double Normalized volume. In case symbol is not available in market watch, return 0
     */
    static double NormalizeVolume(const double volume, const string symbol = NULL)
    {
        CSymbolInfo info;
        if (info.Name(symbol))
        {

            int digits = VolumeDigits(symbol);
            double min = info.LotsMin();
            double max = info.LotsMax();
            double temp = MathRound(volume, digits);

            temp = MathMax(temp, min);
            temp = MathMin(temp, max);
            return temp;
        }

        return 0;
    }

    /**
     * @brief Get the volume step's digits of symbol
     * 
     * @param symbol 
     * @return volume digits. In case symbol is not available in market watch, return 0
     */
    static int VolumeDigits(const string symbol = NULL)
    {
        CSymbolInfo info;
        if (info.Name(symbol))
        {
            double step = info.LotsStep();
            return MathAbs(MathLog10(step));
        }

        return 0;
    }

    /**
     * @brief Calculate point value of symbol
     * 
     * @param symbol 
     * @return point value, In case symbol is not available in market watch, return 0
     */
    static double PointValue(const string symbol = NULL)
    {
        CSymbolInfo info;
        if (info.Name(symbol))
        {
            double point = info.Point();
            double tickValue = info.TickValue();
            double tickSize = info.TickSize();
            double pointValue = tickValue * point / tickSize;
            return pointValue;
        }

        return 0;
    }

    /**
    * @brief Normalize price with digits of symbol
    * 
    * @param price Price
    * @param symbol Input symbol
    * @return double Normalized price. In case symbol is not available in market watch, return 0
    */
    static double NormalizePrice(const double price, const string symbol = NULL)
    {
        CSymbolInfo info;
        if (info.Name(symbol))
        {
            return info.NormalizePrice(price);
        }

        return 0;
    }

    /**
     * @brief Calculate distance points of entry and stop loss.
     * 
     * @param entry Entry point
     * @param sl Stop loss point
     * @param symbol Input symbol
     * @return long Distance points, In case symbol is not available in market watch, return 0
     */
    static long RiskPeriod(const double entry, const double sl, const string symbol = NULL)
    {
        CSymbolInfo info;
        if (info.Name(symbol))
        {
            double point = info.Point();
            double period = MathAbs(entry - sl);
            return period / point;
        }

        return 0;
    }
};
