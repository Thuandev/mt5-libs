#include <Trade/Trade.mqh>
#include "../Common/Symbol.mqh"

class CFxceTrade
{
private:
    CFxceTrade() {}
    ~CFxceTrade() {}

public:
    /**
     * @brief Calculate volume for order 
     * 
     * @param entry Entry point.
     * @param sl Stop loss point.
     * @param risk Money for risk.
     * @param symbol Input symbol.
     * @return double Volume of risk. If entry = stop loss or 
     * symbol is not available in market watch, return 0.
     */
    static double CalculateVolume(double entry, double sl, double risk, string symbol = NULL)
    {
        long riskPoint = CFxceSymbol::RiskPeriod(entry, sl, symbol);
        if (riskPoint == 0)
            return 0;

        double pointValue = CFxceSymbol::PointValue();
        double volume =  CFxceSymbol::NormalizeVolume(risk / (riskPoint * pointValue), symbol);
        return volume;
    }
};